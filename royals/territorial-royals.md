---
title: The Landed Nobility of Drachenwald
exerpt: Lands held in fief to the Crown of Drachenwald
---
<p align="center">
<a href="{{ site.baseurl }}{% link royals/index.html %}" class="btn btn--primary">King & Queen</a>
{% comment %}
<a href="{{ site.baseurl }}{% link royals/heirs.html %}" class="btn btn--primary">The Heirs</a>
{% endcomment %}
<a href="#" class="btn btn--inverse">Landed Nobility</a>
<a href="{{ site.baseurl }}{% link royals/drachenwald-succession.md %}" class="btn btn--primary">Drachenwald Succession</a>
</p>

Across Drachenwald, certain lands are secured by local nobility who hold regions in fief to the Crown.

## Nordmark (Sweden)

<img src="{{ site.baseurl }}{% link images/heraldry/nm_vapen_liten.gif %}" width="40" alt="Arms of Nordmark">{: .align-left}  

Their Highnesses, Prince & Princess of The [Principality of Nordmark](http://www.nordmark.org/)

Furste Stigot Eke  
<script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,102,117,114,115,116,101,64,110,111,114,100,109,97,114,107,46,111,114,103,39,62,102,117,114,115,116,101,64,110,111,114,100,109,97,114,107,46,111,114,103,60,47,97,62));</script>

Furstinna Jovi Torstensdottir  
<script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,102,117,114,115,116,105,110,110,97,64,110,111,114,100,109,97,114,107,46,111,114,103,39,62,102,117,114,115,116,105,110,110,97,64,110,111,114,100,109,97,114,107,46,111,114,103,60,47,97,62));</script>

### Styringheim (Island of Gotland, SE)

<img src="{{ site.baseurl }}{% link images/heraldry/styringheim_logo.gif %}" width="40" alt="Arms of Styringheim">{: .align-left}  

Their Excellencies, Baron & Baroness of the [Barony of Styringheim](https://www.styringheim.se/)  

Baron Edmund (Rasmus Wanneby)  
<script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,98,97,114,111,110,64,115,116,121,114,105,110,103,104,101,105,109,46,115,101,39,62,98,97,114,111,110,64,115,116,121,114,105,110,103,104,101,105,109,46,115,101,60,47,97,62));</script>

Barones Ejsarves Märta (Ellen Norrby)  
<script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,98,97,114,111,110,101,115,115,97,64,115,116,121,114,105,110,103,104,101,105,109,46,115,101,39,62,98,97,114,111,110,101,115,115,97,64,115,116,121,114,105,110,103,104,101,105,109,46,115,101,60,47,97,62));</script>

### Gotvik (Göteborg, SE)  

<img src="{{ site.baseurl }}{% link images/heraldry/gotviktrans.gif %}" width="40" alt="Arms of Gotvik">{: .align-left}  

Their Excellencies, Baron & Baroness of the [Barony of Gotvik](http://www.gotvik.se/)

Baron Olof Vinter  
<script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,98,97,114,111,110,64,103,111,116,118,105,107,46,115,101,39,62,98,97,114,111,110,64,103,111,116,118,105,107,46,115,101,60,47,97,62));</script>

Baroness Katarina Krognos af Ystad  
<script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,98,97,114,111,110,101,115,115,64,103,111,116,118,105,107,46,115,101,39,62,98,97,114,111,110,101,115,115,64,103,111,116,118,105,107,46,115,101,60,47,97,62));</script>

## Insulae Draconis (UK, IE, IS)  

<img src="{{ site.baseurl }}{% link images/heraldry/idarms.gif %}" width="40" alt="Arms of Insulae Draconis">{: .align-left}  

Their Highnesses, Prince & Princess of the [Principality of Insulae Draconis](http://www.insulaedraconis.org/)  

Prince Duncan   
<script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,112,114,105,110,99,101,64,105,110,115,117,108,97,101,100,114,97,99,111,110,105,115,46,111,114,103,39,62,112,114,105,110,99,101,64,105,110,115,117,108,97,101,100,114,97,99,111,110,105,115,46,111,114,103,60,47,97,62));</script>

Princess Constanza  
<script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,112,114,105,110,99,101,115,115,64,105,110,115,117,108,97,101,100,114,97,99,111,110,105,115,46,111,114,103,39,62,112,114,105,110,99,101,115,115,64,105,110,115,117,108,97,101,100,114,97,99,111,110,105,115,46,111,114,103,60,47,97,62));</script>
 
### Knight's Crossing (most of DE)  

<img src="{{ site.baseurl }}{% link images/heraldry/knightscrossing_m.gif %}" width="40" alt="Arms of Knight's Crossing">{: .align-left}  

Their Excellencies, Baron & Baroness of the [Barony of Knight's Crossing](http://www.knightscrossing.org/)  
 
Baron Wolf von Cleve (Jochen Träm)  
<script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,98,97,114,111,110,64,107,110,105,103,104,116,115,99,114,111,115,115,105,110,103,46,111,114,103,39,62,98,97,114,111,110,64,107,110,105,103,104,116,115,99,114,111,115,115,105,110,103,46,111,114,103,60,47,97,62));</script>

Baroness Catalina de Zaragoza (Petra Träm)  
<script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,98,97,114,111,110,105,110,64,107,110,105,103,104,116,115,99,114,111,115,115,105,110,103,46,111,114,103,39,62,98,97,114,111,110,105,110,64,107,110,105,103,104,116,115,99,114,111,115,115,105,110,103,46,111,114,103,60,47,97,62));</script>
 
### Aarnimetsä (FI)

<img src="{{ site.baseurl }}{% link images/heraldry/arnimetsa.gif %}" width="40" alt="Arms of Aarnimetsä">{: .align-left}  

Their Excellencies, Baron & Baroness of the [Barony of Aarnimetsä](http://www.aarnimetsa.org/)
 
Paroni Dubhghall mac Ébhearáird (Henri Laine)  
<script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,112,97,114,111,110,105,64,97,97,114,110,105,109,101,116,115,97,46,111,114,103,39,62,112,97,114,111,110,105,64,97,97,114,110,105,109,101,116,115,97,46,111,114,103,60,47,97,62));</script>
 
Paronitar Mór inghean Bhriain (Maria Laine)  
<script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,112,97,114,111,110,105,116,97,114,64,97,97,114,110,105,109,101,116,115,97,46,111,114,103,39,62,112,97,114,111,110,105,116,97,114,64,97,97,114,110,105,109,101,116,115,97,46,111,114,103,60,47,97,62));</script>
