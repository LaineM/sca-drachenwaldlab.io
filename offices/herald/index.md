---
title: Kingdom Herald
excerpt: Heralds bring alive the pageantry of the medieval era
---

Heralds in the Middle Ages were specialists in chivalry, tournaments, coats-of-arms and the technicalities of war.  Heralds in the Society are experts in communicating tournament combat, court ceremony, and the specifics of historical names and coats-of-arms.

Heralds bring pageantry to the Society: we help make a modern experience turn into a near-medieval one. We do this in several ways.

# Learn about heraldry

* [Heraldry in the SCA]({{ site.baseurl }}{% link offices/herald/heraldryinthesca.md %}), and how it makes our events special
* [Before the Internet...there were heralds]({{ site.baseurl }}{% link offices/herald/heraldsbeforetheinternet.md %}), recording honours and heraldry of great houses

# Welcome to Drachenwald
* [Kingdom Awards and Orders]({{ site.baseurl }}{% link offices/herald/drachenwaldawardsorders.md %}), which explains the honours the Crown can give out
* [Society awards and recommendations]({{ site.baseurl }}{% link offices/herald/society-awards-personal-view.md %}), a personal view about what awards do, and how to recommend people
* [Drachenwald Order of Precedence, which includes a form for award recommendations](http://op.drachenwald.sca.org/op), the record of honours given
* [Send a correction]({{ site.baseurl }}{% link offices/herald/submitacorrectiontoop.md %}) to the Posthorn Herald, if you spot mistakes or gaps in the order of precedence
* [Drachenwald customs: some inter-kingdom anthropology]({{ site.baseurl }}{% link offices/herald/customsandinterkingdomanthropology.md %}), for those who move here from other lands

# Court and protocol
* [Drachenwald Book of Ceremonies, version 3, Jan 2017, AS 51]({{ site.baseurl }}{% link offices/herald/files/ceremonies-v3.pdf %})
* [Drachenwald Book of Ceremonies Apocrypha, Jan 2017, AS 51 (examples of ceremonies)]({{ site.baseurl }}{% link offices/herald/files/drachenwald_apocrypha.pdf %})
* [The Zen of good court: building the special occasions of the Society]({{ site.baseurl }}{% link offices/herald/zenofgoodcourtguidance.md %})
* [Field heralding crash course]({{ site.baseurl }}{% link offices/herald/fieldheraldrycrashcourse.md %})
* [Tournament herald-in-charge checklist]({{ site.baseurl }}{% link offices/herald/heraldinchargetourneychecklist.md %}), to help heralds-in-charge keep track of a full day of tasks 
* [Submit a court report]({{ site.baseurl }}{% link offices/herald/sendacourtreport.md %}), recording the events of court, for kingdom and regional records, as part of kingdom history

# Register your name and armory
* [Submitting names and heraldry in Drachenwald]({{ site.baseurl }}{% link offices/herald/submittingnamesheraldry.md %}), explaining the process of getting your own name and heraldry registered
* [LoARs: Archive of Letters of Acceptance and Return](http://heraldry.sca.org/loar/), the final decisions on submissions from Laurel Sovereign of Arms and staff

# Learning and commenting on heraldry in Drachenwald: tools and documents 

* [SENA, Standards for Evaluation of Names and Armory](http://heraldry.sca.org/sena.html) the rules all heralds use to review submissions
* [OSCAR, Online System for Commentary and Response](https://oscar.sca.org/index.php?action=181), , where heralds gather online
* [SCA Armorial](http://oanda.sca.org/), every herald's favourite database
* [Morsulus Herald's precedent searches](http://www.morsulus.org/)
* [Sofya la Rus' name and heraldry searches](http://sofyalarus.info/searches.html)
* [East Kingdom Herald University](https://elmet.eastkingdom.org/ekhu/)

# Society-wide College of Arms

* [SCA College of Arms on the Web](http://heraldry.sca.org/welcome.html)
* [Administrative handbook](http://heraldry.sca.org/admin.html): defines what we register, what we protect, our submission procedures and heraldic responsibilities in the SCA

{% include officer-contacts.html %}